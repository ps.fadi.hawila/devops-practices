from openjdk:latest
RUN mkdir -p /u01/docker \
    && chmod 777 -R /u01
COPY /target/*.jar /u01/docker/dockerpractice.jar
EXPOSE 8090
CMD java -jar -Dspring.profiles.active=h2 /u01/docker/dockerpractice.jar
